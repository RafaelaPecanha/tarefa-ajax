const url = 'http://treinamento-ajax-api.herokuapp.com/messages/'
const botaoEnviar = document.querySelector(".botao-enviar")
 // Pega o histórico de mensagens
const msgHist = document.querySelector(".mensagens")
botaoEnviar.addEventListener("click", (event) => {
   
    const messageInput = document.querySelector('.texto')
    const body = {
       message: {
           content: messageInput.value,
           author: "Rafaela Peçanha"
       }
    }
   const config = {
       method: 'POST',
       body: JSON.stringify(body),
       headers: {"Content-Type": "application/json"}
   }

   fetch(url, config)
   .then(response => response.json())
    .then(message => {
        createPostDiv(message)   
    })
    .catch((erro) => {
        alert("Aconteceu um erro")
    })
})

function createPostDiv (message){
     // Pega o texto da caixa
     const li = document.createElement("li")
     li.className = "mensagem"
     li.innerHTML = `
            <h1> ${message.author}</h1>
            <p class="texto-msg"> ${message.content}</p>
             <div class="botoes">
                 <input type="button" value="Editar" class="Editar botao-msg" onclick="editar(this)">
                 <input type="button" value="Deletar" class="Deletar botao-msg" onclick="deletar(this,${message.id})">
             </div>
             <div class="botoes-modo-editar" hidden>
                 <input>
                 <input type="button" value="Enviar" class="Enviar botao-msg" onclick="editarTexto(this, ${message.id})">
             </div>
             `
    
 
     msgHist.appendChild(li)
 }




function deletar(element,id) {

    const config = {
        method: "DELETE"
    }

    fetch(url + id, config)
    .then(() => {
        element.parentNode.parentNode.remove()
    }) 
    .catch((erro) => {
        alert("Aconteceu erro")
    })   
};

function editar(elemento) {
    const parent = elemento.parentNode.parentNode

    // Esconder os botões normais
   parent.querySelector(".botoes").toggleAttribute("hidden")

    // Mostrar os botões de edição
    parent.querySelector(".botoes-modo-editar").toggleAttribute("hidden")

}

function editarTexto(elemento, id) {

    const parent = elemento.parentNode.parentNode
    const textArea = elemento.previousElementSibling
   
   const body = {
    message: {
        content: textArea.value
    } 
}
    
    const fetchConfig = {
    method: "PUT",
    headers:{"Content-Type": "application/json"},
    body: JSON.stringify(body)

}  
    
    fetch(url + id, fetchConfig)
    .then(response => response.json())
    .then(() => {
        parent.querySelector(".texto-msg").innerText = textArea.value
        parent.querySelector(".botoes").toggleAttribute("hidden")
        parent.querySelector(".botoes-modo-editar").toggleAttribute("hidden")
        textArea.value = ""
    }) 
    .catch((erro) => {
        alert("Aconteceu erro")
    })   
    
}

const getMessages = () => {
    fetch(url)
    .then(response => response.json())
    .then(messages => {
        messages.forEach(message => {
            createPostDiv(message)
            
        });
    })
    .catch((erro) => {
        alert("Aconteceu um erro")
    })
}
getMessages()

